#!/usr/bin/env bash

# Ref: https://zookeeper.apache.org/doc/current/zookeeperStarted.html#sc_InstallingSingleMode

set -e

# Install dependencies
apt-get update && apt-get install -y curl wget tmux vim openjdk-17-jre python3-pip pipx

# Setup zookeeper
mkdir -p /opt/zookeeper
wget -nv -O - https://dlcdn.apache.org/zookeeper/zookeeper-3.9.1/apache-zookeeper-3.9.1-bin.tar.gz | tar -xz -C /opt/zookeeper --strip-components 1

cat >> /etc/environment << EOF
JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
ZOO_LOG_DIR=/var/log/zookeeper
SERVER_JVMFLAGS=-Djava.net.preferIPv4Stack=true
EOF

cat > /etc/profile.d/zookeeper.sh <<EOF
export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
export PATH=\${PATH}:/opt/zookeeper/bin
EOF

useradd -r -s /usr/sbin/nologin -d /var/lib/zookeeper zookeeper
mkdir -p /var/lib/zookeeper
mkdir -p /var/log/zookeeper
chown -R zookeeper:zookeeper /var/lib/zookeeper
chown -R zookeeper:zookeeper /var/log/zookeeper

cat > /opt/zookeeper/conf/zoo.cfg << EOF
tickTime=2000
initLimit=10
syncLimit=5
dataDir=/var/lib/zookeeper
clientPort=2181
maxClientCnxns=60
autopurge.snapRetainCount=3
autopurge.purgeInterval=1
metricsProvider.className=org.apache.zookeeper.metrics.prometheus.PrometheusMetricsProvider
metricsProvider.httpHost=0.0.0.0
metricsProvider.httpPort=7000
metricsProvider.exportJvmInfo=true
server.1=znode1:2888:3888
server.2=znode2:2888:3888
server.3=znode3:2888:3888
EOF

cat > /etc/systemd/system/zookeeper.service <<EOF
[Unit]
Description=Apache Zookeeper Server

[Service]
Type=forking
User=zookeeper
Group=zookeeper
ExecStart=/opt/zookeeper/bin/zkServer.sh start
ExecStop=/opt/zookeeper/bin/zkServer.sh stop
EnvironmentFile=/etc/environment
PIDFile=/var/lib/zookeeper/zookeeper_server.pid
GuessMainPID=no
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload

# Install zk-shell zookeeper client
mkdir -p /var/lib/pipx
PIPX_HOME=/var/lib/pipx PIPX_BIN_DIR=/usr/local/bin pipx install zk-shell

# Convenience settings
cat > /etc/vim/vimrc <<EOF
runtime! debian.vim
let g:skip_defaults_vim = 1
syntax on
set background=dark
filetype plugin indent off
set showmatch
set ignorecase
set incsearch
set mouse-=a
EOF

update-alternatives --set editor /usr/bin/vim.basic

systemctl disable unattended-upgrades --now
apt-get purge -y unattended-upgrades
